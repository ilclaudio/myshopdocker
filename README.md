# MyShopDocker: Your e-commerce on a Docker container #

This is an e-commerce built with WooCommerce and Wordpress you can use to test your plugins for Wordpress or WooCommerce.


### Components ###
* Wordpress 5.8.2
* WooCommerce 5.9.0
* StoreFront 3.9.1
* PHP 7.4.3
* Mariadb 10.3.31
* Adminer 4.8.1

### Version ###
The last [stable version](https://bitbucket.org/ilclaudio/myshopdocker/src/master/VERSION.txt).

### Platform ###
The project was built using Docker Desktop on Windows 10 and GitExtensions for Windows.

### Installation ###

```
git clone https://bitbucket.org/ilclaudio/myshopdocker.git
cd myshopdocker
docker build -t myshop-img -f Dockerfile .
docker run -p 80:80 -p 3306:3306 --name=myshop -d myshop-img
```

### How to ###

* How to view the shop page?
```
http://localhost/myshop/
```

* How tyo login as Wordpress administrator?
```
http://localhost/wp-admin/

Username: manager 
Password: password
```
* How to connect to the database on the container using a Mysql client on my pc?
```
Configure your client with these parameters:
username: admin
password: admin
host:     127.0.0.1
database: myshop

```

* Ho to login into the shell of the container? 
```
docker exec -it myshop /bin/bash
```
* How to login into the database using the shell?
```
docker exec -it myshop /bin/bash
mysql -u admin -padmin
```

* How to log in into Adminer?
```
Visit the page:
http://localhost/adminer.php

Configure your client with these parameters:
System: Mysql
Server: 127.0.0.1
username: admin
password: admin
database: myshop

```

### Project ###
The page of the project is: [MyshopDocker](https://bitbucket.org/ilclaudio/myshopdocker/src/master/) on BitBucket.
### License ###

This code is under the [GPLv3](https://bitbucket.org/ilclaudio/myshopdocker/src/master/LICENSE.txt) or later license.


### Gallery ###
![Image1](docs/screenshots/01-VSC_BuildImage.png) **Image 1:** How to build the image of the container with VSC
![Image2](docs/screenshots/05-MyShopManagerLogin.png) **Image 2:** How to login in the backoffice
![Image3](docs/screenshots/06-MyShopAdminInterface.png) **Image 3:** The backoffice
in the backoffice
![Image4](docs/screenshots/07-MyShopHP.png) **Image 4:** The shop
![Image5](docs/screenshots/08-MyShopCart.png) **Image 5:** The shop cart
![Image6](docs/screenshots/09-MyShopCheckout.png) **Image 6:** The shop checkout
![Image7](docs/screenshots/11-LoginAdminer.png) **Image 7:** Log into Adminer

